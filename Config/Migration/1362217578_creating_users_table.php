<?php
class CreatingUsersTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'users' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary'
					),
					'username' => array(
						'type'    =>'string',
						'length' => 255,
						'null'    => false,
						'default' => NULL
					),
					'password' => array(
						'type'    =>'string',
						'length' => 40,
						'null'    => false,
					),
					'group_id' => array(
						'type' => 'integer',
						'null' => false,
					),
					'created' => array(
						'type' => 'datetime',
					),
					'modified' => array(
						'type' => 'datetime',
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
						'username' => array(
							'column' => 'username',
							'unique' => 1
						),
					),
				),
				'groups' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary'
					),
					'name' => array(
						'type'    =>'string',
						'length' => 100,
						'null'    => false,
					),
					'created' => array(
						'type' => 'datetime',
					),
					'modified' => array(
						'type' => 'datetime',
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'users',
				'groups',
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
