<?php
class CreatingAclTables extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */



	public $migration = array(
		'up' => array(
			'create_table' => array(
				'acos' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary',
					),
					'parent_id' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'model' => array(
						'type'    =>'string',
						'length' => 255,
						'default' => '',
					),
					'foreign_key' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'alias' => array(
						'type'    =>'string',
						'length' => 255,
						'default' => '',
					),
					'lft' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'rght' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
				'aros_acos' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary',
					),
					'aro_id' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'aco_id' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'_create' => array(
						'type'    =>'string',
						'length' => 2,
						'null' => false,
						'default' => 0,
					),
					'_read' => array(
						'type'    =>'string',
						'length' => 2,
						'null' => false,
						'default' => 0,
					),
					'_update' => array(
						'type'    =>'string',
						'length' => 2,
						'null' => false,
						'default' => 0,
					),
					'_delete' => array(
						'type'    =>'string',
						'length' => 2,
						'null' => false,
						'default' => 0,
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
				'aros' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary',
					),
					'parent_id' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'model' => array(
						'type'    =>'string',
						'length' => 255,
						'default' => '',
					),
					'foreign_key' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'alias' => array(
						'type'    =>'string',
						'length' => 255,
						'default' => '',
					),
					'lft' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'rght' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'acos',
				'aros_acos',
				'aros',
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
