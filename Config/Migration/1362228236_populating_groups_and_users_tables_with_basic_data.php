<?php
App::uses('Controller', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('AuthComponent', 'Controller/Component');
App::uses('AclComponent', 'Controller/Component');
App::uses('AroComponent', 'Controller/Component');

class PopulatingGroupsAndUsersTablesWithBasicData extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
		),
		'down' => array(
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		$Group = ClassRegistry::init('Group');
		$User = ClassRegistry::init('User');

		if ($direction == 'up') {
			$controller = new Controller(new CakeRequest());
			$collection = new ComponentCollection();
			$this->Acl = new AclComponent($collection);
			$this->Acl->startup($controller);			

			/* create admin and its group */
			$Group->create(array('name' => 'Administrators'));
			$Group->save();

			/* allow admins to do everything */
			$this->Acl->allow($Group, 'controllers');

			$User->create(array(
				'username' => 'admin',
				'password' => 'poker',
				'group_id' => $Group->id,
			));
			$User->save();

			/* create a user and its group */
			$Group->create(array('name' => 'Users'));
			$Group->save();

			/* deny users from doing anything */
			$this->Acl->deny($Group, 'controllers');

			$User->create(array(
				'username' => 'user',
				'password' => 'poker',
				'group_id' => $Group->id,
			));
			$User->save();

		} else {
			$Group->deleteAll(array(
				'OR' => array(
					array('name' => 'Administrators'),
					array('name' => 'Users')
				),
			), true, true);

			$User->deleteAll(array(
				'OR' => array(
					array('username' => 'admin'),
					array('username' => 'user')
				),
			), false);
		}
		return true;
	}
}
