<?php
class CreatingBoardsTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'boards' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary'
					),
					'lesson_id' => array(
						'type' => 'integer',
						'null' => false,
					),
					'pot' => array(
						'type' => 'integer',
						'null' => false,
					),
					'created' => array(
						'type' => 'datetime',
					),
					'modified' => array(
						'type' => 'datetime',
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'boards',
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
