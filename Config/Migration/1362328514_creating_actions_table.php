<?php
class CreatingActionsTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'actions' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary'
					),
					'lesson_id' => array(
						'type' => 'integer',
						'null' => false,
					),
					'type' => array(
						'type' => 'integer',
						'null' => false,
					),
					'position' => array(
						'type' => 'integer',
						'null' => false,
					),
					'amount' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'parent_id' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'lft' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'rght' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'created' => array(
						'type' => 'datetime',
					),
					'modified' => array(
						'type' => 'datetime',
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'actions',
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
