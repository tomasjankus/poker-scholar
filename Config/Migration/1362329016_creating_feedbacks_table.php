<?php
class CreatingFeedbacksTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'feedbacks' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary'
					),
					'action_id' => array(
						'type' => 'integer',
						'null' => false,
					),
					'amount_min' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'amount_max' => array(
						'type' => 'integer',
						'default' => NULL,
					),
					'name' => array(
						'type'    =>'string',
						'length' => 255,
						'null'    => false,
						'default' => NULL
					),
					'description' => array(
						'type'    =>'text',
						'default' => NULL
					),
					'continue' => array(
						'type' => 'boolean',
						'default' => false,
					),
					'penalty' => array(
						'type' => 'integer',
						'default' => 100,
					),
					'created' => array(
						'type' => 'datetime',
					),
					'modified' => array(
						'type' => 'datetime',
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'feedbacks',
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
