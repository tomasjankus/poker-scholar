<?php
App::uses('AclExtras', 'AclExtras.Lib');

class UpdatingAcos extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
		),
		'down' => array(
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		$this->AclExtras = new AclExtras();
		$this->AclExtras->startup();
		$this->AclExtras->Shell = new Shell();

		$Aco = ClassRegistry::init('Aco');

		$this->AclExtras->aco_sync(NULL);
		return true;
	}
}
