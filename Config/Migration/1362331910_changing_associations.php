<?php
class ChangingAssociations extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'drop_table' => array(
				'boards',
				'cards',
			),
			'rename_field' => array(
				'seats' => array(
					'board_id' => 'lesson_id'
				),
			),
		),
		'down' => array(
			'create_table' => array(
				'boards' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary'
					),
					'lesson_id' => array(
						'type' => 'integer',
						'null' => false,
					),
					'pot' => array(
						'type' => 'integer',
						'null' => false,
					),
					'created' => array(
						'type' => 'datetime',
					),
					'modified' => array(
						'type' => 'datetime',
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
				'cards' => array(
					'id' => array(
						'type' => 'integer',
						'null' => false,
						'default' => NULL,
						'key' => 'primary'
					),
					'board_id' => array(
						'type' => 'integer',
						'null' => false,
					),
					'value' => array(
						'type' => 'integer',
						'null' => false,
					),
					'created' => array(
						'type' => 'datetime',
					),
					'modified' => array(
						'type' => 'datetime',
					),
					'indexes' => array(
						'PRIMARY' => array(
							'column' => 'id',
							'unique' => 1
						),
					),
				),
			),
			'rename_field' => array(
				'seats' => array(
					'lesson_id' => 'board_id'
				),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
