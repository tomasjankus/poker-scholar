<?php
class AddingPotAndCardsToTheLesson extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_field' => array(
				'lessons' => array(
					'pot' => array(
						'type' => 'integer',
						'default' => 0,
					),
					'hero_cards' => array(
						'type' => 'string',
						'length' => 16,
						'default' => '',
					),
					'flop_cards' => array(
						'type' => 'string',
						'length' => 16,
						'default' => '',
					),
					'turn_card' => array(
						'type' => 'string',
						'length' => 16,
						'default' => '',
					),
					'river_card' => array(
						'type' => 'string',
						'length' => 16,
						'default' => '',
					),
				),
			),		
		),
		'down' => array(
			'drop_field' => array(
				'lessons' => array(
					'pot',
					'hero_cards',
					'flop_cards',
					'turn_card',
					'river_card',
				),
			),		
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
