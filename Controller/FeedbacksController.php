<?php
App::uses('AppController', 'Controller');
/**
 * Feedbacks Controller
 *
 * @property Feedback $Feedback
 */
class FeedbacksController extends AppController {

	public $components = array('RequestHandler');
	public $helpers = array('Backbone.Backbone');

	public function beforeFilter() {
		$this->Auth->allow();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		if ($action_id = $this->params['url']['id']) { /* ajax request with action_id */
			$amount = $this->params['url']['amount'];
			$conditions = array(
				'action_id' => $action_id,
				'AND' => array(
					array('OR' => array('amount_min' => null, 'amount_min <=' => $amount)),
					array('OR' => array('amount_max' => null, 'amount_max >=' => $amount)),
				)
			);
			$this->set('feedback', $this->Feedback->find('first', compact('conditions')));
		} else { /* regular index request */
			$this->Feedback->recursive = 0;
			$this->set('feedbacks', $this->paginate());
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
		$this->set('feedback', $this->Feedback->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Feedback->create();
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$actions = $this->Feedback->Action->find('list');
		$this->set(compact('actions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
			$this->request->data = $this->Feedback->find('first', $options);
		}
		$actions = $this->Feedback->Action->find('list');
		$this->set(compact('actions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Feedback->id = $id;
		if (!$this->Feedback->exists()) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Feedback->delete()) {
			$this->Session->setFlash(__('Feedback deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Feedback was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
