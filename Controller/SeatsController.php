<?php
App::uses('AppController', 'Controller');
/**
 * Seats Controller
 *
 * @property Seat $Seat
 */
class SeatsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Seat->recursive = -1;
		$this->set('seats', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Seat->exists($id)) {
			throw new NotFoundException(__('Invalid seat'));
		}
		$options = array('conditions' => array('Seat.' . $this->Seat->primaryKey => $id));
		$this->set('seat', $this->Seat->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Seat->create();
			if ($this->Seat->save($this->request->data)) {
				$this->Session->setFlash(__('The seat has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The seat could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$lessons = $this->Seat->Lesson->find('list');
		$this->set(compact('lessons'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Seat->exists($id)) {
			throw new NotFoundException(__('Invalid seat'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Seat->save($this->request->data)) {
				$this->Session->setFlash(__('The seat has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The seat could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Seat.' . $this->Seat->primaryKey => $id));
			$this->request->data = $this->Seat->find('first', $options);
		}
		$lessons = $this->Seat->Lesson->find('list');
		$this->set(compact('lessons'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Seat->id = $id;
		if (!$this->Seat->exists()) {
			throw new NotFoundException(__('Invalid seat'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Seat->delete()) {
			$this->Session->setFlash(__('Seat deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Seat was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
