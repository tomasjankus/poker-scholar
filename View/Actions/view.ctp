
<div id="page-container" class="row-fluid">

	<div id="sidebar" class="span3">
		
		<div class="actions">
			
			<ul class="nav nav-list bs-docs-sidenav">			
						<li><?php echo $this->Html->link(__('Edit Action'), array('action' => 'edit', $action['Action']['id']), array('class' => '')); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Action'), array('action' => 'delete', $action['Action']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $action['Action']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Actions'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Action'), array('action' => 'add'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lessons'), array('controller' => 'lessons', 'action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lesson'), array('controller' => 'lessons', 'action' => 'add'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('List Actions'), array('controller' => 'actions', 'action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Action'), array('controller' => 'actions', 'action' => 'add'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('List Feedbacks'), array('controller' => 'feedbacks', 'action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Feedback'), array('controller' => 'feedbacks', 'action' => 'add'), array('class' => '')); ?> </li>
				
			</ul><!-- .nav nav-list bs-docs-sidenav -->
			
		</div><!-- .actions -->
		
	</div><!-- #sidebar .span3 -->
	
	<div id="page-content" class="span9">
		
		<div class="actions view">

			<h2><?php  echo __('Action'); ?></h2>

			<table class="table table-striped table-bordered">
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Lesson'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($action['Lesson']['name'], array('controller' => 'lessons', 'action' => 'view', $action['Lesson']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Type'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['type']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Position'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['position']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Amount'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['amount']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Parent Action'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($action['ParentAction']['id'], array('controller' => 'actions', 'action' => 'view', $action['ParentAction']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Lft'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['lft']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Rght'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['rght']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($action['Action']['modified']); ?>
			&nbsp;
		</td>
</tr>			</table><!-- .table table-striped table-bordered -->
			
		</div><!-- .view -->

					
			<div class="related">

				<h3><?php echo __('Related Actions'); ?></h3>
				
				<?php if (!empty($action['ChildAction'])): ?>
				
					<table class="table table-striped table-bordered">
						<tr>
									<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Lesson Id'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Position'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Lft'); ?></th>
		<th><?php echo __('Rght'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
							<?php
								$i = 0;
								foreach ($action['ChildAction'] as $childAction): ?>
		<tr>
			<td><?php echo $childAction['id']; ?></td>
			<td><?php echo $childAction['lesson_id']; ?></td>
			<td><?php echo $childAction['type']; ?></td>
			<td><?php echo $childAction['position']; ?></td>
			<td><?php echo $childAction['amount']; ?></td>
			<td><?php echo $childAction['parent_id']; ?></td>
			<td><?php echo $childAction['lft']; ?></td>
			<td><?php echo $childAction['rght']; ?></td>
			<td><?php echo $childAction['created']; ?></td>
			<td><?php echo $childAction['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'actions', 'action' => 'view', $childAction['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'actions', 'action' => 'edit', $childAction['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'actions', 'action' => 'delete', $childAction['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $childAction['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
					</table><!-- .table table-striped table-bordered -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Child Action'), array('controller' => 'actions', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- .actions -->
				
			</div><!-- .related -->

					
			<div class="related">

				<h3><?php echo __('Related Feedbacks'); ?></h3>
				
				<?php if (!empty($action['Feedback'])): ?>
				
					<table class="table table-striped table-bordered">
						<tr>
									<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Action Id'); ?></th>
		<th><?php echo __('Amount Min'); ?></th>
		<th><?php echo __('Amount Max'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Continue'); ?></th>
		<th><?php echo __('Penalty'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
							<?php
								$i = 0;
								foreach ($action['Feedback'] as $feedback): ?>
		<tr>
			<td><?php echo $feedback['id']; ?></td>
			<td><?php echo $feedback['action_id']; ?></td>
			<td><?php echo $feedback['amount_min']; ?></td>
			<td><?php echo $feedback['amount_max']; ?></td>
			<td><?php echo $feedback['name']; ?></td>
			<td><?php echo $feedback['description']; ?></td>
			<td><?php echo $feedback['continue']; ?></td>
			<td><?php echo $feedback['penalty']; ?></td>
			<td><?php echo $feedback['created']; ?></td>
			<td><?php echo $feedback['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'feedbacks', 'action' => 'view', $feedback['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'feedbacks', 'action' => 'edit', $feedback['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'feedbacks', 'action' => 'delete', $feedback['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $feedback['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
					</table><!-- .table table-striped table-bordered -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Feedback'), array('controller' => 'feedbacks', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- .actions -->
				
			</div><!-- .related -->

			
	</div><!-- #page-content .span9 -->

</div><!-- #page-container .row-fluid -->
