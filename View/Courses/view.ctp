
<div id="page-container" class="row-fluid">

	<div id="sidebar" class="span3">
		
		<div class="actions">
			
			<ul class="nav nav-list bs-docs-sidenav">			
				<?php if ($this->Access->check(array('controller' => 'courses', 'action' => 'edit'))) { ?>
					<li><?php echo $this->Html->link(__('Edit Course'), array('action' => 'edit', $course['Course']['id']), array('class' => '')); ?> </li>
					<li><?php echo $this->Form->postLink(__('Delete Course'), array('action' => 'delete', $course['Course']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $course['Course']['id'])); ?> </li>
					<li><?php echo $this->Html->link(__('List Courses'), array('action' => 'index'), array('class' => '')); ?> </li>
					<li><?php echo $this->Html->link(__('New Course'), array('action' => 'add'), array('class' => '')); ?> </li>
					<li><?php echo $this->Html->link(__('List Lessons'), array('controller' => 'lessons', 'action' => 'index'), array('class' => '')); ?> </li>
					<li><?php echo $this->Html->link(__('New Lesson'), array('controller' => 'lessons', 'action' => 'add'), array('class' => '')); ?> </li>
				<?php } ?>
			</ul><!-- .nav nav-list bs-docs-sidenav -->
			
		</div><!-- .actions -->
		
	</div><!-- #sidebar .span3 -->
	
	<div id="page-content" class="span9">
		<div class="courses view">
			<h2><?php  echo __('Course'); ?></h2>
			<table class="table table-striped table-bordered">
				<?php if ($this->Access->check(array('controller' => 'courses', 'action' => 'edit'))) { ?>
					<tr>
						<td><strong><?php echo __('Id'); ?></strong></td>
						<td><?php echo h($course['Course']['id']); ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td><strong><?php echo __('Name'); ?></strong></td>
					<td><?php echo h($course['Course']['name']); ?></td>
				</tr>
				<tr>
					<td><strong><?php echo __('Description'); ?></strong></td>
					<td><?php echo h($course['Course']['description']); ?></td>
				</tr>
				<tr>
					<td><strong><?php echo __('Difficulty'); ?></strong></td>
					<td><?php echo h($course['Course']['difficulty']); ?></td>
				</tr>
				<?php if ($this->Access->check(array('controller' => 'courses', 'action' => 'edit'))) { ?>
					<tr>
						<td><strong><?php echo __('Created'); ?></strong></td>
						<td><?php echo h($course['Course']['created']); ?></td>
					</tr>
					<tr>
						<td><strong><?php echo __('Modified'); ?></strong></td>
						<td><?php echo h($course['Course']['modified']); ?></td>
					</tr>
				<?php } ?>
			</table><!-- .table table-striped table-bordered -->
		</div><!-- .view -->

		<div class="related">
			<h3><?php echo __('Lessons Included'); ?></h3>
			<?php if (!empty($course['Lesson'])) { ?>
				<table class="table table-striped table-bordered">
				<tr>
					<?php if ($this->Access->check(array('controller' => 'lessons', 'action' => 'edit'))) { ?>
						<th><?php echo __('Id'); ?></th>
					<?php } ?>
					<th><?php echo __('Name'); ?></th>
					<th><?php echo __('Description'); ?></th>
					<?php if ($this->Access->check(array('controller' => 'lessons', 'action' => 'edit'))) { ?>
						<th><?php echo __('Course Id'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					<?php } ?>
				</tr>
				<?php
					$i = 0;
					foreach ($course['Lesson'] as $lesson) { 
				?>
					<tr>
						<?php if ($this->Access->check(array('controller' => 'lessons', 'action' => 'edit'))) { ?>
							<td><?php echo $lesson['id']; ?></td>
						<?php } ?>
						<td><?php echo $lesson['name']; ?></td>
						<td><?php echo $lesson['description']; ?></td>
						<?php if ($this->Access->check(array('controller' => 'lessons', 'action' => 'edit'))) { ?>
							<td><?php echo $lesson['course_id']; ?></td>
							<td><?php echo $lesson['created']; ?></td>
							<td><?php echo $lesson['modified']; ?></td>
							<td class="actions">
								<?php echo $this->Html->link(__('View'), array('controller' => 'lessons', 'action' => 'view', $lesson['id']), array('class' => 'btn btn-mini')); ?>
								<?php echo $this->Html->link(__('Edit'), array('controller' => 'lessons', 'action' => 'edit', $lesson['id']), array('class' => 'btn btn-mini')); ?>
								<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'lessons', 'action' => 'delete', $lesson['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $lesson['id'])); ?>
							</td>
						<?php } ?>
					</tr>
				<?php } // end foreach ?>
				</table><!-- .table table-striped table-bordered -->
			<?php } // endif; ?>

				
			<div class="actions">
				<?php echo $this->Html->link(__('Start the Course').$this->Html->tag('i', '', array('class' => 'icon-play icon-white')), array('controller' => 'lessons', 'action' => 'take', $course['Lesson'][0]['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
			</div><!-- .actions -->
		</div><!-- .related -->
	</div><!-- #page-content .span9 -->
</div><!-- #page-container .row-fluid -->
