
<div id="page-container" class="row-fluid">

	<div id="sidebar" class="span3">
		
		<div class="actions">
			
			<ul class="nav nav-list bs-docs-sidenav">			
						<li><?php echo $this->Html->link(__('Edit Feedback'), array('action' => 'edit', $feedback['Feedback']['id']), array('class' => '')); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Feedback'), array('action' => 'delete', $feedback['Feedback']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $feedback['Feedback']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Feedbacks'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Feedback'), array('action' => 'add'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('List Actions'), array('controller' => 'actions', 'action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Action'), array('controller' => 'actions', 'action' => 'add'), array('class' => '')); ?> </li>
				
			</ul><!-- .nav nav-list bs-docs-sidenav -->
			
		</div><!-- .actions -->
		
	</div><!-- #sidebar .span3 -->
	
	<div id="page-content" class="span9">
		
		<div class="feedbacks view">

			<h2><?php  echo __('Feedback'); ?></h2>

			<table class="table table-striped table-bordered">
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Action'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($feedback['Action']['id'], array('controller' => 'actions', 'action' => 'view', $feedback['Action']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Amount Min'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['amount_min']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Amount Max'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['amount_max']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Description'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['description']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Continue'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['continue']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Penalty'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['penalty']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($feedback['Feedback']['modified']); ?>
			&nbsp;
		</td>
</tr>			</table><!-- .table table-striped table-bordered -->
			
		</div><!-- .view -->

			
	</div><!-- #page-content .span9 -->

</div><!-- #page-container .row-fluid -->
