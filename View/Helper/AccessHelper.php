<?php  
class AccessHelper extends AppHelper { 
	var $acos = array(); 
	var $helpers = array('Html'); 

	function __construct() { 
		App::import('Component', 'Acl'); 
		App::import('Component', 'Session'); 

		$collection = new ComponentCollection();
		$this->Acl = new AclComponent($collection); 
		$this->Session = new SessionComponent($collection); 
	}

	function check($options = array()) { 
		$acoExists = true; 

		$user = $this->Session->read('Auth.User.id'); 
		$acos = $this->Session->read('Auth.Acos'); 

		if (isset($options['controller']))
			$controller = Inflector::camelize($options['controller']); 
		else
			$controller = Inflector::camelize($this->params['controller']); 

		$action = $options['action']; 

		if (!isset($acos[$controller][$action])) { 
			$controllerAco = $this->Acl->Aco->find('first', array('recursive' => 0, 'conditions' => array('alias' => $controller))); 

			if ($cid = $controllerAco['Aco']['id']) { 
				$actionAco = $this->Acl->Aco->find('count',
					array('recursive' => 0,
						'conditions' => array('parent_id' => $cid,'alias' => $action),
					)
				); 
				if($actionAco < 1) $acoExists = false; 
			} else $acoExists = false; 

			if ((!empty($user)) && ($acoExists && $this->Acl->check(array('model' => 'User', 'foreign_key' => $user), $controller.'/'.$action) || (!$acoExists))) { 
				$acos[$controller][$action] = true; 
			} else { 
				$acos[$controller][$action] = false;     
			} 

			$this->Session->write('Auth.Acos', $acos); 
		} 
		return $acos[$controller][$action];
	} 
} 
