<?php
	$backbonify = $this->Backbone->backbonify(
		$lesson,
		'Lesson',
		array(
			'contain' => array(
				'Seats' => 'Seat',
				'Actions' => 'Action',
			),
		)
	);

	echo json_encode($backbonify);
?>