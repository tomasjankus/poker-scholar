<?php
	/* backbone app */
	echo $this->Html->script('app/app.js', array('inline' => false));

	/* backbone templates */
	echo $this->Html->script('app/templates.js', array('inline' => false));

	/* output lesson data */
	echo $this->Backbone->bootstrap(
		$lesson,
		'Lesson',
		array(
			'varName' => 'App.Data.Lesson',
			'contain' => array(
				'Seats' => 'Seat',
				'Actions' => 'Action',
			),
		)
	);

	/* backbone app files */
	echo $this->Html->script('app/models/seat.js', array('inline' => false));
	echo $this->Html->script('app/models/action.js', array('inline' => false));
	echo $this->Html->script('app/models/feedback.js', array('inline' => false));
	echo $this->Html->script('app/collections/seats.js', array('inline' => false));
	echo $this->Html->script('app/collections/actions.js', array('inline' => false));
	echo $this->Html->script('app/views/seat.js', array('inline' => false));
	echo $this->Html->script('app/views/seats.js', array('inline' => false));
	echo $this->Html->script('app/views/action.js', array('inline' => false));
	echo $this->Html->script('app/views/actions.js', array('inline' => false));
	echo $this->Html->script('app/views/feedback.js', array('inline' => false));


	echo $this->Html->script('app/models/lesson.js', array('inline' => false));
	echo $this->Html->script('app/views/lesson.js', array('inline' => false));

	/* start the app */
	echo $this->Html->script('app/init.js', array('inline' => false));

	echo $this->Html->script('libs/jquery-ui-1.10.1.custom.min.js', array('inline' => false));

	/* load css */
	echo $this->Html->css('lessons/layout.css', null, array('inline' => false));
	echo $this->Html->css('lessons/lesson.css', null, array('inline' => false));

	echo $this->Html->css('libs/jquery-ui-1.10.1.custom.min.css', null, array('inline' => false));
?>

<div id="page-container" class="container">
	<div id="page-content" class="row-fluid">
		<div class="span6">
			<h3>
				<?php echo h($lesson['Lesson']['name']); ?>
			</h3>
			<div id="lesson-content" class="well">

			</div>
			<div id="input-container" class="well row-fluid">

			</div>
		</div>
		<div id="info-container" class="span3">
			<div class="well scrollable">
				<div>
					<h4 id="description-title">
						Description
					</h4>
					<span id="description-content">
						<?php echo h($lesson['Lesson']['description']); ?>
					</span>
				</div>
			</div>
			<div class="well scrollable">
				<div id="feedback-container">

				</div>
			</div>
		</div>
		<div id="sidebar" class="span3">
			<ul class="nav nav-list bs-docs-sidenav">			
				<li><?php echo $this->Html->link(__('Return to the Course'), array('controller' => 'courses', 'action' => 'view', $lesson['Course']['id']), array('class' => '')); ?> </li>
			</ul><!-- .nav nav-list bs-docs-sidenav -->
		</div>
	</div>
</div>
