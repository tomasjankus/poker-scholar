
<div id="page-container" class="row-fluid">
	<div id="sidebar" class="span3">
		<h2><?php  echo __('Courses'); ?></h2>
		<ul class="nav nav-list bs-docs-sidenav">	
		<?php
			foreach ($courses as $course_id => $course_name) {
				echo $this->Html->tag('li', $this->Html->link($course_name, array('controller' => 'courses', 'action' => 'view', $course_id), array('class' => '')));
			}
		?>
		</ul><!-- .nav nav-list bs-docs-sidenav -->
	</div><!-- #sidebar .span3 -->
	<div id="page-content" class="span9">

			
	</div><!-- #page-content .span9 -->

</div><!-- #page-container .row-fluid -->
