
<div id="page-container" class="row-fluid">

	<div id="sidebar" class="span3">
		
		<div class="actions">
			
			<ul class="nav nav-list bs-docs-sidenav">			
						<li><?php echo $this->Html->link(__('Edit Seat'), array('action' => 'edit', $seat['Seat']['id']), array('class' => '')); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Seat'), array('action' => 'delete', $seat['Seat']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $seat['Seat']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Seats'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Seat'), array('action' => 'add'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lessons'), array('controller' => 'lessons', 'action' => 'index'), array('class' => '')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lesson'), array('controller' => 'lessons', 'action' => 'add'), array('class' => '')); ?> </li>
				
			</ul><!-- .nav nav-list bs-docs-sidenav -->
			
		</div><!-- .actions -->
		
	</div><!-- #sidebar .span3 -->
	
	<div id="page-content" class="span9">
		
		<div class="seats view">

			<h2><?php  echo __('Seat'); ?></h2>

			<table class="table table-striped table-bordered">
				<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($seat['Seat']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Lesson'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($seat['Lesson']['name'], array('controller' => 'lessons', 'action' => 'view', $seat['Lesson']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Position'); ?></strong></td>
		<td>
			<?php echo h($seat['Seat']['position']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($seat['Seat']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Chips'); ?></strong></td>
		<td>
			<?php echo h($seat['Seat']['chips']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($seat['Seat']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($seat['Seat']['modified']); ?>
			&nbsp;
		</td>
</tr>			</table><!-- .table table-striped table-bordered -->
			
		</div><!-- .view -->

			
	</div><!-- #page-content .span9 -->

</div><!-- #page-container .row-fluid -->
