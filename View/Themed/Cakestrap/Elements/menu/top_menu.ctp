<div class="navbar">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="nav-collapse">
				<ul class="nav">
					<li><?php echo $this->Html->link('Home', '/'); ?></li>

					<?php if ($this->Access->check(array('controller' => 'users', 'action' => 'index'))) { ?>
						<li><?php echo $this->Html->link('Users', array('controller'=>'users', 'action'=>'index')); ?></li>
					<?php } ?>

					<?php if ($this->Access->check(array('controller' => 'profiles', 'action' => 'index'))) { ?>
						<li><?php echo $this->Html->link('Profiles', array('controller'=>'profiles', 'action'=>'index')); ?></li>
					<?php } ?>

					<?php if ($this->Access->check(array('controller' => 'groups', 'action' => 'index'))) { ?>
						<li><?php echo $this->Html->link('Groups', array('controller'=>'groups', 'action'=>'index')); ?></li>
					<?php } ?>

					<?php if ($this->Session->read('Auth.User')) { ?>
						<li><?php echo $this->Html->link('My Profile', array('controller'=>'profiles', 'action'=>'update')); ?></li>
					<?php } ?>

					<li><?php
						if ($this->Session->read('Auth.User')) {
							echo $this->Html->link('Logout', array('controller'=>'users', 'action'=>'logout')); 
						} else {
							echo $this->Html->link('Login', array('controller'=>'users', 'action'=>'login')); 
						}
					?></li>
				</ul>
			</div>
		</div>
	</div>
</div>