<div id="page-container" class="row-fluid">

	<div id="page-content" class="span12">

		<div class="users form">

			<?php echo $this->Form->create('User', array('action' => 'login')); ?>
				<fieldset>
					<h2><?php echo __('Login'); ?></h2>
				<?php
					echo "<div class='control-group'>";
					echo $this->Form->input('username', array('class' => 'span3'));
					echo "</div>";
					echo "<div class='control-group'>";
					echo $this->Form->input('password', array('class' => 'span3'));
					echo "</div>";
				?>
				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
			<?php echo $this->Form->end(); ?>
			
		</div>
			
	</div><!-- #page-content .span9 -->

</div><!-- #page-container .row-fluid -->
