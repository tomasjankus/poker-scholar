(function() {
	window.App = {
		Models: {},
		Views: {},
		Collections: {},
		Templates: {},
		Data: {},
		Const: {}
	};

	vent = _.extend({}, Backbone.Events);

	App.Const.SPEED = 0;

	App.Const.FIRST_ACTION = -1;

	App.Const.POST_SB = 1;
	App.Const.POST_BB = 2;
	App.Const.FOLD = 3;
	App.Const.CHECK = 4;
	App.Const.CALL = 5;
	App.Const.BET = 6; /* TODO */
	App.Const.RAISE = 7;

	App.Const.DEAL = 100; /* TODO */
	App.Const.FLOP = 101; /* TODO */
	App.Const.TURN = 102; /* TODO */
	App.Const.RIVER = 103; /* TODO */

	App.Const.BTN_FOLD = 203; /* TODO */
	App.Const.BTN_CHECK = 204; /* TODO */
	App.Const.BTN_CALL = 205; /* TODO */
	App.Const.BTN_BET = 206; /* TODO */
	App.Const.BTN_RAISE = 207; /* TODO */

	App.Const.BTN_CLASS = {
		203: "btn-inverse btn-fold",
		204: "btn-info btn-check",
		205: "btn-success btn-call",
		206: "btn-warning btn-bet",
		207: "btn-danger btn-raise",
	};

	App.Const.BTN_NAME = {
		203: "Fold",
		204: "Check",
		205: "Call",
		206: "Bet",
		207: "Raise to",
	};

})();
