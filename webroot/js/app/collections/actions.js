(function() {
	App.Collections.Actions = Backbone.Collection.extend({
		model: App.Models.Action,

		initialize: function (models, options) {
			if (options)
				this.lesson = options.lesson;
		}
	});
})();