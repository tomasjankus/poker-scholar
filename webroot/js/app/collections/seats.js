(function() {
	App.Collections.Seats = Backbone.Collection.extend({
		model: App.Models.Seat,

		initialize: function (models, options) {
			this.lesson = options.lesson;
		}

	});
})();