$(document).ready(function() {
	lesson = new App.Models.Lesson(App.Data.Lesson);
	lessonView = new App.Views.Lesson({ model: lesson });

	actions = new App.Collections.Actions();
	actionsView = new App.Views.Actions({ collection: actions });

	feedbackView = new App.Views.Feedback();

	vent.trigger('action:play', {'continue' : 'true', 'action_id' : App.Const.FIRST_ACTION});
});
