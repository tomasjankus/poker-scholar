(function() {
	App.Models.Action = Backbone.Model.extend({
		defaults: {
			id: 0,
			lesson_id: 0,
			type: 0,
			position: 0,
			parent_id: null,
			lft: null,
			rght: null,
			amount: 0,
		},

		urlRoot: '../../actions',

		initialize: function() {
			this.set('min_amount', this.get('amount'));
			this.set('title', App.Const.BTN_NAME[this.get('type')]);
		}
	});
})();
