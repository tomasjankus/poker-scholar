(function() {
	App.Models.Feedback = Backbone.Model.extend({
		defaults: {
			name: 'Loading',
			description: 'Please wait...',
		},

		urlRoot: '../../feedbacks',

	});
})();
