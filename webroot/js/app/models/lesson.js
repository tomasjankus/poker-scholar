(function() {
	App.Models.Lesson = Backbone.Model.extend({
		defaults: {
			id: 0,
			name: '',
			description: '',
			course_id: 0,
			pot: 0
		},

		urlRoot: '../../lessons',

		initialize: function (model) {
			this.Seats = new App.Collections.Seats(model.Seats, {lesson: this});
			this.Actions = new App.Collections.Actions(model.Actions, {lesson: this});
		}
	});
})();
