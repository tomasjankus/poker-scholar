(function() {
	App.Models.Seat = Backbone.Model.extend({
		defaults: {
			id: 0,
			lesson_id: 0,
			position: 0,
			name: '',
			chips: 0,
			stake: 0,
			action: ''
		},

		urlRoot: '../../seats',
	});
})();