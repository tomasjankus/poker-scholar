(function() {

	App.Templates.seatView = [
		'<span class="name"><%= name %></span>',
		'<span class="chips">$<%= chips %></span>',
		'<span class="action"><%= action %></span>',
		'<span class="stake amount-<%= stake %>"><small>$<%= stake %></small></span>',
	].join('');

	App.Templates.seatsView = [
		'',
	].join('');

	App.Templates.actionView = [
		'<%= title %><span class="amount-<%= amount %>"> $<%= amount %></span>',
	].join('');

	App.Templates.actionsView = [
		'<div class="span7 action-buttons"></div>',
		'<div class="span5">',
			'<div class="row-fluid">',
				'<div class="span4 bet-min" id="min"><a href="#">Min</a></div>',
				'<div class="span4 bet-pot" id="pot"><a href="#">Pot</a></div>',
				'<div class="span4 bet-max" id="max"><a href="#">Max</a></div>',
			'</div>',
			'<div class="row-fluid">',
				'<div id="slider"></div>',
			'</div>',
		'</div>',
	].join('');

	App.Templates.feedbackView = [
		'<h4 id="feedback-title"><%= name %></h4>',
		'<span id="feedback-content"><%= description %></span>',
	].join('');

	App.Templates.lessonView = [
		'<div class="table-container">',
			'<span class="pot">Pot: $<span><%= pot %></span></span>',
			'<button class="btn btn-inverse replay"><i class="icon-repeat icon-white"></i> Replay</button>',
		'</div>',
	].join('');

	/* wrap templates in underscore template() function */
	for (temp in App.Templates) {
		if (App.Templates.hasOwnProperty(temp))
			App.Templates[temp] = _.template(App.Templates[temp]);
	}

})();
