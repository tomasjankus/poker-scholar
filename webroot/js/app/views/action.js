(function() {
	App.Views.Action = Backbone.View.extend({

		template: App.Templates.actionView,
		tagName: 'button',
		className: 'btn',

		events: {
			'click': 'feedback',
		},

		initialize: function() {
			this.model.on('change:amount', this.render, this);

			this.$el.addClass(App.Const.BTN_CLASS[this.model.get('type')]);
			actionType = this.model.get('type');
			if (actionType == App.Const.BTN_RAISE || actionType == App.Const.BTN_BET) {
				$('#slider').slider({
					'min': parseInt(this.model.get('min_amount')),
					'max': parseInt(this.model.get('min_amount')),
					'step': parseInt(lesson.get('BB')),
					'slide': this.changeBet,
					'change': this.changeBet
				});
				vent.on('setBet', this.setBet, this);
				vent.trigger('slider:setMax', {seat: this.model.get('position')});
			} else if (actionType == App.Const.BTN_CALL || actionType == App.Const.BTN_CHECK) {
				lesson.set('toCall', this.model.get('amount'));
			}
		},

		changeBet: function( event, ui ) {
			vent.trigger('setBet', {value: ui.value});
		},

		setBet: function( options ) {
			this.model.set('amount', options.value);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},

		feedback: function() {
			vent.trigger('feedback:get', this.model);
		}
	});
})();
