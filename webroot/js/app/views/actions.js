(function() {
	App.Views.Actions = Backbone.View.extend({

		template: App.Templates.actionsView,
		el: '#input-container',

		initialize: function() {
			vent.on('action:showButtons', this.render, this);
		},

		events: {
			'click .bet-min': 'setBet',
			'click .bet-pot': 'setBet',
			'click .bet-max': 'setBet',
		},

		render: function () {
			this.$el.html(this.template(this.collection.toJSON()));
			this.collection.each(function(action) {
				this.$el.find('.action-buttons').append(new App.Views.Action({ model: action }).render().el);
			}, this);

			return this;
		},

		setBet: function(e) {
			e.preventDefault();
			this.$el.find('#slider').slider('option', 'value', $('#slider').slider('option', arguments[0].currentTarget.id));
		}

	});
})();
