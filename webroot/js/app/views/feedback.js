(function() {
	App.Views.Feedback = Backbone.View.extend({

		template: App.Templates.feedbackView,
		model: new App.Models.Feedback,
		el: '#feedback-container',

		initialize: function() {
			vent.on('feedback:get', this.getFeedback, this);
			this.model.on('change', this.render, this);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},

		getFeedback: function(action) {
			this.model.clear({ 'silent' : true }).set(this.model.defaults);
			this.model.fetch({
				'data': action.toJSON(),
				'error': function() {
					console.log('An error occured while fetching feedback data.');
				}
			});
		}
	});
})();
