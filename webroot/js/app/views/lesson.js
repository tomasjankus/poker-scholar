(function() {
	App.Views.Lesson = Backbone.View.extend({
		
		template: App.Templates.lessonView,
		el: '#lesson-content',

		events: {
			'click .replay': 'replay'
		},

		initialize: function() {
			this.model.on('change:pot', this.renderPot, this);
			vent.on('action:play', this.playAction, this);
			this.render();
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			this.$el.find('.table-container').append(new App.Views.Seats({ collection: this.model.Seats }).render().el);
			return this;
		},

		renderPot: function() {
			this.$el.find('.pot span').html(this.model.get('pot'));
		},

		playAction: function (options) {
			if (!options.action_id || !options.continue) {
				vent.trigger('action:showButtons');
				this.$el.find('.replay').show();
				return;
			} else if (options.action_id == App.Const.FIRST_ACTION) {
				this.nextAction = this.model.Actions.where({ 'lft' : null })[0];
			} else {
				this.nextAction = this.model.Actions.get(options.action_id);
			}

			console.log(this.nextAction.id);

			options.seat = this.nextAction.get('position');
			options.action_id = this.nextAction.get('rght');

			if (options.action_id)
				options.next_seat = this.model.Actions.get(options.action_id).get('position');
			else
				options.next_seat = null;

			options.amount = this.nextAction.get('amount');

			var actionType = this.nextAction.get('type');

			if (actionType <= App.Const.RAISE)
				this.model.set('pot', parseInt(this.model.get('pot')) + parseInt(options.amount));

			if (actionType == App.Const.POST_SB) {
				vent.trigger('seat:postSb', options);
			} else if (actionType == App.Const.POST_BB) {
				vent.trigger('seat:postBb', options);
			} else if (actionType == App.Const.FOLD) {
				vent.trigger('seat:fold', options);
			} else if (actionType == App.Const.CHECK) {
				vent.trigger('seat:check', options);
			} else if (actionType == App.Const.CALL) {
				vent.trigger('seat:call', options);
			} else if (actionType == App.Const.RAISE) {
				vent.trigger('seat:raise', options);
			} else if (actionType >= App.Const.BTN_FOLD && actionType <= App.Const.BTN_RAISE) {
				actions.add(this.nextAction);
				vent.trigger('action:play', options);
			} else {
				vent.trigger('action:play', options);
			}
		},

		replay: function() {
			lesson.set(App.Data.Lesson);
			lesson.Seats.update(App.Data.Lesson.Seats);
			lesson.Actions.update(App.Data.Lesson.Actions);

			actionsView.$el.html('');
			$('#slider').slider('destroy');

			vent.trigger('seat:reset');
			vent.trigger('action:play', {'continue' : 'true', 'action_id' : App.Const.FIRST_ACTION});
		}

	});
})();
