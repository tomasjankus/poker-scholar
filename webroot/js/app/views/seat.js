(function() {
	App.Views.Seat = Backbone.View.extend({

		template: App.Templates.seatView,
		className: 'single-seat well',

		initialize: function() {
			this.model.on('change', this.render, this);

			this.model.on('active', this.setActive, this);
			vent.on('seat:reset', this.resetState, this);

			this.model.on('posted', this.animatePost, this);
			this.model.on('folded', this.animateFold, this);
			this.model.on('checked', this.animate, this);
			this.model.on('called', this.animate, this);
			this.model.on('raised', this.animateRaise, this);

			this.$el.addClass('seat-' + this.model.get('position'));
		},

		setActive: function(options) {
			this.$el.addClass('active');
		},

		resetState: function() {
			this.$el.removeClass('active');
			this.$el.removeAttr('style');
			this.model.set({ 'stake': 0, 'action' : '' });
			console.log('reseting');
		},

		animatePost: function(options) {
			if (options.next_seat)
				vent.trigger('seat:active', options);

			this.$el.removeClass('active');
			this.$el.find('.stake').addClass('blinds');

			this.$el.find('.action')
				.show()
				.delay(3000*App.Const.SPEED)
				.fadeOut(400, function() {
					vent.trigger('action:play', options);
				});
		},

		animateFold: function(options) {
			if (options.next_seat)
				vent.trigger('seat:active', options);

			this.$el.removeClass('active');

			this.$el.find('.action')
				.show();
			this.$el
				.delay(3000*App.Const.SPEED)
				.fadeTo(400, 0.2, function() {
					vent.trigger('action:play', options);
				});
		},

		animate: function(options) {
			if (options.next_seat)
				vent.trigger('seat:active', options);

			this.$el.removeClass('active');
			this.$el.find('.stake').removeClass('blinds raise');

			this.$el.find('.action')
				.show()
				.delay(3000*App.Const.SPEED)
				.fadeOut(400, function() {
					vent.trigger('action:play', options);
				});
		},

		animateRaise: function(options) {
			this.animate(options);
			this.$el.find('.stake').addClass('raise');
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});
})();
