(function() {
	App.Views.Seats = Backbone.View.extend({

		template: App.Templates.seatsView,
		className: 'table-container',

		initialize: function() {
			vent.on('seat:active', this.setActive, this)
			vent.on('seat:postSb', this.postSb, this)
			vent.on('seat:postBb', this.postBb, this)
			vent.on('seat:fold', this.fold, this)
			vent.on('seat:check', this.check, this)
			vent.on('seat:call', this.call, this)
			vent.on('seat:raise', this.raise, this)

			vent.on('slider:setMax', this.setMax, this)

			this.$el.addClass('max-players-' + this.maxPlayers());
		},

		render: function () {
			this.$el.html(this.template(this.collection.toJSON()));
			this.collection.each(function(seat) {
				this.$el.append(new App.Views.Seat({ model: seat }).render().el);
			}, this);
			return this;
		},

		setActive: function(options) {
			var seat = this.collection.where({ 'position' : options.next_seat })[0];
			seat.trigger('active', options);
		},

		postSb: function(options) {
			var seat = this.collection.where({ 'position' : options.seat })[0];
			seat.set({
				'chips' : seat.get('chips') - options.amount,
				'stake' : parseInt(seat.get('stake')) + parseInt(options.amount),
				'action' : 'Posts SB'
			});
			seat.trigger('posted posted:SB', options);
		},

		postBb: function(options) {
			var seat = this.collection.where({ 'position' : options.seat })[0];
			lesson.set('BB', parseInt(options.amount));
			seat.set({
				'chips' : seat.get('chips') - options.amount,
				'stake' : parseInt(seat.get('stake')) + parseInt(options.amount),
				'action' : 'Posts BB'
			});
			seat.trigger('posted posted:BB', options);
		},

		fold: function(options) {
			var seat = this.collection.where({ 'position' : options.seat })[0];
			seat.set({
				'action' : 'Folded'
			});
			seat.trigger('folded', options);
		},

		check: function(options) {
			var seat = this.collection.where({ 'position' : options.seat })[0];
			seat.set({
				'action' : 'Checked'
			});
			seat.trigger('checked', options);
		},

		call: function(options) {
			var seat = this.collection.where({ 'position' : options.seat })[0];
			seat.set({
				'chips' : seat.get('chips') - options.amount,
				'stake' : parseInt(seat.get('stake')) + parseInt(options.amount),
				'action' : 'Called $' + options.amount
			});
			seat.trigger('called', options);
		},

		raise: function(options) {
			var seat = this.collection.where({ 'position' : options.seat })[0];
			var stake = parseInt(seat.get('stake')) + parseInt(options.amount);
			seat.set({
				'chips' : seat.get('chips') - options.amount,
				'stake' : stake,
				'action' : 'Raised to $' + stake
			});
			seat.trigger('raised', options);
		},

		maxPlayers: function () {
			if (this.collection.length < 3)
				return '2';
			else if (this.collection.length < 7)
				return '6';
			else if (this.collection.length < 10)
				return '9';
			else
				return '10';
		},

		setMax: function(options) {
			var seat = this.collection.where({ 'position' : options.seat })[0];
			$('#slider').slider('option', 'pot', parseInt(seat.get('stake')) + lesson.get('toCall')*2 + lesson.get('pot'));
			$('#slider').slider('option', 'max', parseInt(seat.get('chips')) + seat.get('stake'));
		}
	});
})();
